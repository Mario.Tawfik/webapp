FROM node:slim
WORKDIR /app
COPY helloworld-js* ./
RUN npm install
ENTRYPOINT ["npm start"]
EXPOSE 3000
